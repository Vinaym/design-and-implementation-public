```
          (         )            (        (     
 (        )\ )   ( /(            )\ )     )\ )  
 )\ )    (()/(   )\())      (   (()/(    (()/(  
(()/(     /(_)) ((_)\       )\   /(_))    /(_)) 
 /(_))_  (_))     ((_)   _ ((_) (_))     (_))_| 
(_)) __| | _ \   / _ \  | | | | | _ \    | |_   
  | (_ | |   /  | (_) | | |_| | |  _/    | __|  
   \___| |_|_\   \___/   \___/  |_|      |_|    
                                                
                                 (                      (         *             
                                 )\ )           (       )\ )    (  `            
                                (()/(   (       )\     (()/(    )\))(    (      
                                 /(_))  )\   ((((_)(    /(_))  ((_)()\   )\     
                                (_))   ((_)   )\ _ )\  (_))_   (_()((_) ((_)    
                                | _ \  | __|  (_)_\(_)  |   \  |  \/  | | __|   
                                |   /  | _|    / _ \    | |) | | |\/| | | _|    
                                |_|_\  |___|  /_/ \_\   |___/  |_|  |_| |___|   
                                                
                                                        
```

Description
-----------------------------------------------------------------

Our project aims to automate many of the manual aspects to a farm. 
It will tap into the full potential allowing you to automate 
everything from your feeders to your irrigation system. 




Requirements
-----------------------------------------------------------------
• Group_F_UGRAD_Design.zip<br/>
• A computer that can run java<br />
• Java version 7.0 or above
                           


                                      
Installation
-----------------------------------------------------------------

1. Unzip Archive to specified folder
2. Run the GUI.java file to launch the interface.
3. Once inside the interface GUI be sure to input a string to name the item 
   you are creating, and it will open a window for the user to input other 
   important details to better allow systems AI to better understand your farm.
4. Once entered, click visualize to obtain your farm visualization.




Contributing Developers
-----------------------------------------------------------------

Adam Hoskins<br />
Jospeh McKinley<br />
Robert Rummell<br />
Tiana Terrell<br />
Vinay Modi<br />
