import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
 
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
 
import java.net.URL;
import java.io.IOException;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
 
public class ItemTree extends JPanel
                      implements TreeSelectionListener {
   
	static FarmComponent itemContainer = new ItemContainer("",0,0,0,0,0);
	static FarmComponent item = new Item("",0,0,0,0,0);

	static DefaultMutableTreeNode top =
            new DefaultMutableTreeNode("Farm Items");
	
	private static DefaultMutableTreeNode previousTop;
	
	private static int currIndex = 0;
	
	private static int itemContainerFlag = 0;
	private static int itemFlag = 0;
	
	
    private JTree tree;
   
    //Line styles.
    private static boolean playWithLineStyle = false;
    private static String lineStyle = "Horizontal";
     
    //Optionally set the look and feel.
    private static boolean useSystemLookAndFeel = false;
 
    public ItemTree() {
        super(new GridLayout(1,0));
 
     
        //Create a tree that allows one selection at a time.
        tree = new JTree(top);
        tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);
 
        //Listen for when the selection changes.
        tree.addTreeSelectionListener(this);
 
        if (playWithLineStyle) {
            System.out.println("line style = " + lineStyle);
            tree.putClientProperty("JTree.lineStyle", lineStyle);
        }
 
        //Create the scroll pane and add the tree to it. 
        JScrollPane treeView = new JScrollPane(tree);
 
      
        JScrollPane infoView = new JScrollPane(null);
 
        //Add the scroll panes to a split pane.
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setLeftComponent(treeView);
        splitPane.setRightComponent(infoView);

        splitPane.setPreferredSize(new Dimension(500, 300));
 
        //Add the split pane to this panel.
        add(splitPane);
        
    }
 
    /** Required by TreeSelectionListener interface. */
    public void valueChanged(TreeSelectionEvent e) {
        
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
    	previousTop = top;
    	
        if (node == null) return;
        
        if (node instanceof Item) {
            /*Do Nothing.
             * Items cannot hold new Items!*/
        	currIndex = node.getIndex(node) + 1;
        } else {
        
        	top = node;
        	currIndex = node.getIndex(node) + 1;
        }
    }
    
    static void deleteNode(){
    	previousTop.remove(currIndex);

    }
 

    public void addNewFarmComponent(FarmComponent fm){
    	
    	if(fm instanceof Item){
    		item = fm;
    		itemFlag = 1;
    		createNodes();
		}
		else{
			itemContainer = fm;
			itemContainerFlag = 1;
			createNodes();
		}
    	
    	
    }
    
    void createNodes() {
        DefaultMutableTreeNode category = null;

        
        // Takes care of the rest of the new Tree Node. 
        if(itemFlag == 1 && top instanceof ItemContainer){
        	category = new DefaultMutableTreeNode(item);
            top.add(category);
            itemFlag = 0;
        }
        else if(itemFlag == 1 && top instanceof Item){
        	throw new UnsupportedOperationException("Adding an Item to an Item is NOT allowed!");
        }
        else if(itemContainerFlag == 1 && top instanceof ItemContainer){
        	category = new DefaultMutableTreeNode(itemContainer);
            top.add(category);
            itemContainerFlag = 0;
        }
        else if(itemContainerFlag == 1 && top instanceof Item){
        	throw new UnsupportedOperationException("Adding an Item to an Item Container is NOT allowed!");
        } 
        // Takes care of possible initial new Tree Node as an ItemContainer.
        else if(itemContainerFlag == 1){
        	category = new DefaultMutableTreeNode(itemContainer);
            top.add(category);
            itemContainerFlag = 0;
        }// Takes care of possible initial new Tree Node as an Item.
        else if(itemFlag == 1 ){
        	category = new DefaultMutableTreeNode(item);
            top.add(category);
            itemFlag = 0;
        }else{
        	throw new UnsupportedOperationException();
        }

    }
         
   
}
