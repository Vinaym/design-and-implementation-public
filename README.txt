Our official ReadMe is located at: https://gitlab.com/Vinaym/design-and-implementation-public/edit/master/README.md

Our code is located at: https://gitlab.com/Vinaym/design-and-implementation-public.git


Instructions for running our code:

After downloading the files, the GUI class is the one that links all of our classes together to create a graphical interface
for a user to create a visualization of a farm using items, item containers, and farmComponents.

Run the GUI class.


Further or extra Instructions: 
1. Unzip Archive to specified folder
.
2. Run the GUI.java file in compiler to launch the interface.

3. Once inside the interface GUI be sure to input a string to name the item
 
you are creating, and it will open a window for the user to input other 
important details to better allow systems AI to better understand your farm.

4. Once entered, click visualize to obtain your farm visualization.