import javax.swing.tree.DefaultMutableTreeNode;

public abstract class FarmComponent extends DefaultMutableTreeNode{
	

		
		public void add(FarmComponent newFarmComponent) {
			throw new UnsupportedOperationException();
		}
		
		public void remove(FarmComponent newFarmComponent) {
			throw new UnsupportedOperationException();
		}
		
		public FarmComponent getComponent(int componentIndex) {
			throw new UnsupportedOperationException();
		}
		
		public String getName() {
			throw new UnsupportedOperationException();
		}
		
		public String setName() {
			throw new UnsupportedOperationException();
		}
		
		public int getPrice() {
			throw new UnsupportedOperationException();
		}
		
		public int setPrice() {
			throw new UnsupportedOperationException();
		}
		
		public int getLocationX() {
			throw new UnsupportedOperationException();
		}
		
		public int setLocationX() {
			throw new UnsupportedOperationException();
		}
		
		public int getLocationY() {
			throw new UnsupportedOperationException();
		}
		
		public int setLocationY() {
			throw new UnsupportedOperationException();
		}
		
		public int getLength() {
			throw new UnsupportedOperationException();
		}
			
		public int setLength() {
			throw new UnsupportedOperationException();
		}
		
		public int getWidth() {
			throw new UnsupportedOperationException();
		}
		
		public int setWidth() {
			throw new UnsupportedOperationException();
		}
		
		public void displayItemInfo() {
			throw new UnsupportedOperationException();
		}

		public String toString() {

           		throw new UnsupportedOperationException();

		}

	
}
