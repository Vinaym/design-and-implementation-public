import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;


public class VisualizerComponent extends JComponent {
	

	private ArrayList<FarmComponent> itemContainer = new ArrayList<FarmComponent>();

	VisualizerComponent(ArrayList<FarmComponent> ic){
		this.itemContainer = ic;
	}


	
	public void paintComponent(Graphics g){
			
		Graphics2D g2 = (Graphics2D) g;
		
		for(int i = 0; i< itemContainer.size(); i++){
			
			g.drawRect(itemContainer.get(i).getLocationX(), itemContainer.get(i).getLocationY(), 
					itemContainer.get(i).getWidth(), itemContainer.get(i).getLength());
			
			g.drawString(itemContainer.get(i).getName(), itemContainer.get(i).getLocationX() + ( itemContainer.get(i).getWidth()/4),
					itemContainer.get(i).getLocationY() + ( itemContainer.get(i).getLength()/2));
			
			
			
		}
			
	}
}
