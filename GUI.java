import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;


class GUI extends JFrame implements ActionListener {
	
	/*This container allows for several Item Containers to be added to the visualizer screen.*/
	private static ArrayList<FarmComponent> IC = new ArrayList<FarmComponent>();
	
	
	private static String txtInput = "";
	private static int xInput = 0;
	private static int yInput = 0;
	private static int lengthInput = 0;
	private static int widthInput = 0;
	private static int priceInput = 0;

	

	private static int price = 0;
	private static int x = 0;
	private static int y = 0;
	private static int length = 0;
	private static int width = 0;
	private static ItemTree nodes = new ItemTree();
	
	
	private static FarmComponent fm;	
	
    public static void main(String args[])
    {

        //Creating the Frame
        JFrame frame = new JFrame("Interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);

        //Creating the MenuBar and adding components
        JMenuBar mb = new JMenuBar();
        JMenu m1 = new JMenu("File");
        mb.add(m1);
        JMenuItem m10 = new JMenuItem("Visualizer");
        JMenuItem m11 = new JMenuItem("Close");
        m1.add(m10);
        m1.add(m11);
        

        //Creating the panel at bottom and adding components, maybe unneeded
        JPanel panel = new JPanel(); // the panel is not visible in output
        JLabel label = new JLabel("Enter Name of item: ");
        JTextField tf = new JTextField(10); // accepts up to 10 characters
        
        JButton JButtonadd = new JButton("Add");
        JButton JButtondelete = new JButton("Delete");
        JButton Visualize = new JButton("Visualizer");
        JButtonadd.setBounds(0, 400, 100, 30);
        Visualize.setBounds(350, 400, 100, 30);
		 
        
		//adding components to the frame.
        frame.add(mb);
        frame.add(Visualize);
        frame.add(JButtonadd);
    
        //Added the Tree Nodes to the GUI.
    	frame.add(nodes);
    	
        panel.add(label); // Components Added using Flow Layout
        
        panel.add(tf);

        
        
        /*
         * Adding Components to the frame.
         * Appending things to specific spots on the GUI.  
         */
        frame.getContentPane().add(BorderLayout.SOUTH, panel); //bottompanel
        frame.getContentPane().add(BorderLayout.NORTH, mb); //menubar
   
        frame.setVisible(true);
        
        
        //Text field listener
        tf.addKeyListener(new KeyListener() 
        {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}

			@Override //should be able to take text input and use that for appending to visualizer
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//System.out.println(tf.getText()); //this does work
				txtInput =  tf.getText();
				//System.out.println(txtInput);
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        
       
        //CLOSE in menu bar.
        m11.addActionListener(new ActionListener() 
        {
			@Override
			public void actionPerformed(ActionEvent e) {
		        System.exit(0);
		    }
		});
        
        
        //VISUALIZE in menu bar, copy paste from other visualizer.
        m10.addActionListener(new ActionListener()
        {
        	@Override
        	public void actionPerformed(ActionEvent e)
        	{
        		System.out.println("Visualizer Started");

        		IC.add(fm);
        		
        		JFrame frame = new JFrame();
        		VisualizerComponent visualize = new VisualizerComponent(IC);
        		frame.setSize(300, 300);
        		frame.setTitle("Map of the Farm");
        		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        	    
        		frame.add(visualize);
        		frame.setVisible(true);       
             
            }
        });
         
        /*
         * ADD button.
         */
        JButtonadd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				JFrame itemChooserFrame = new JFrame("Please Choose");
				itemChooserFrame.setSize(300, 100);
				//itemChooserFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				JPanel p = new JPanel();
				JButton item = new JButton("Item");
				JButton itemContainer = new JButton("Item Container");
				
				p.add(item);
				p.add(itemContainer);
				itemChooserFrame.getContentPane().add(BorderLayout.SOUTH, p);
				
				itemChooserFrame.setVisible(true);
				
				
				/*
				 * ITEM Button INSIDE the Add Button.
				 */
				item.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						JFrame textBoxFrame = new JFrame();
						textBoxFrame.setSize(700, 150);
						textBoxFrame.setTitle("User Inputs for the Visualizer");

								
						
						//JTextFields and Panels
				        JPanel panel1 = new JPanel(); //Panel that holds all of the textFields
				        
				        //Here are all of the textfields for use
				        JTextField name = new JTextField(10); // accepts up to 10 characters

				        JTextField length = new JTextField(10); // accepts up to 10 characters
				        JTextField width = new JTextField(10); // accepts up to 10 characters
				        JTextField x = new JTextField(10); // accepts up to 10 characters
				        JTextField y = new JTextField(10); // accepts up to 10 characters
				        JTextField price = new JTextField(10); // accepts up to 10 characters
				        JButton Visualize = new JButton("Visualizer");

				      //bottompanel
				        textBoxFrame.getContentPane().add(BorderLayout.SOUTH, Visualize);
				        

				        length.setText("Length");
				        width.setText("Width");
				        x.setText("X-Coordinate");
				        y.setText("Y-Coordinate");
				        price.setText("Price");
				        name.setText(txtInput); 
				       
				        
				        panel1.setSize(700, 100);
				 
				        //Adding the panel to the frame
				        textBoxFrame.add(panel1);

				        //Adds all of the text fields
				        panel1.add(length);
				        panel1.add(width);
				        panel1.add(x);
				        panel1.add(y);
				        panel1.add(price);
				        panel1.add(name);
				        
						textBoxFrame.setVisible(true);
				        
						Visualize.addActionListener( new ActionListener()
					        {
					        	@Override
					        	public void actionPerformed(ActionEvent e)
					        	{
					        		System.out.println("Visualizer Started");

					        		 priceInput = Integer.parseInt(price.getText()); 
					        		
					        		 xInput = Integer.parseInt(x.getText()); 
					        		 yInput = Integer.parseInt(y.getText()); 
					        		 lengthInput = Integer.parseInt(length.getText()); 
					        		 widthInput = Integer.parseInt(width.getText()); 

					        		/*Farm Component*/

					        		fm = new Item(name.getText(), priceInput, xInput, yInput, lengthInput, widthInput); 
					      						    
					        		IC.add(fm);
					        		nodes.addNewFarmComponent(fm);
					        								        		
					        		JFrame frame = new JFrame();
					        		VisualizerComponent visualize = new VisualizerComponent(IC);
					        		frame.setSize(300, 300);
					        		frame.setTitle("Map of the Farm");
    	    
					        		frame.add(visualize);
					        		frame.setVisible(true);       
							           
							    }
									
							});
						
						
					  }
					
				});
				
				
				/*
				 * ITEM Container Button INSIDE the Add Button.
				 */
				itemContainer.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

								
								JFrame textBoxFrame = new JFrame();
								textBoxFrame.setSize(700, 150);
								textBoxFrame.setTitle("User Inputs for the Visualizer");

										
								
								//JTextFields and Panels
						        JPanel panel1 = new JPanel(); //Panel that holds all of the textFields
						        
						        //Here are all of the textfields for use
						        JTextField name = new JTextField(10); // accepts up to 10 characters

						        JTextField length = new JTextField(10); // accepts up to 10 characters
						        JTextField width = new JTextField(10); // accepts up to 10 characters
						        JTextField x = new JTextField(10); // accepts up to 10 characters
						        JTextField y = new JTextField(10); // accepts up to 10 characters
						        JTextField price = new JTextField(10); // accepts up to 10 characters
						        JButton Visualize = new JButton("Visualizer");

						      //bottompanel
						        textBoxFrame.getContentPane().add(BorderLayout.SOUTH, Visualize);
						        

						        length.setText("Length");
						        width.setText("Width");
						        x.setText("X-Coordinate");
						        y.setText("Y-Coordinate");
						        price.setText("Price");
						        name.setText(txtInput); 
						       
						        
						        panel1.setSize(700, 100);
						 
						        //Adding the panel to the frame
						        textBoxFrame.add(panel1);

						        //Adds all of the text fields
						        panel1.add(length);
						        panel1.add(width);
						        panel1.add(x);
						        panel1.add(y);
						        panel1.add(price);
						        panel1.add(name);
						        
								textBoxFrame.setVisible(true);
						        
								Visualize.addActionListener( new ActionListener()
							        {
							        	@Override
							        	public void actionPerformed(ActionEvent e)
							        	{
							        		System.out.println("Visualizer Started");

							        		 priceInput = Integer.parseInt(price.getText()); 
							        		
							        		 xInput = Integer.parseInt(x.getText()); 
							        		 yInput = Integer.parseInt(y.getText()); 
							        		 lengthInput = Integer.parseInt(length.getText()); 
							        		 widthInput = Integer.parseInt(width.getText()); 

							        		/*Farm Component*/

							        		fm = new ItemContainer(name.getText(), priceInput, xInput, yInput, lengthInput, widthInput); 
							      						    
							        		IC.add(fm);
							        		nodes.addNewFarmComponent(fm);
							        								        		
							        		JFrame frame = new JFrame();
							        		VisualizerComponent visualize = new VisualizerComponent(IC);
							        		frame.setSize(300, 300);
							        		frame.setTitle("Map of the Farm");
        	    
							        		frame.add(visualize);
							        		frame.setVisible(true);       
							        	
								
							    }
									
							});
						
						
					  }
					
				});
				
			  }
			
		});
        
        //VISUALIZE button.
        Visualize.addActionListener( new ActionListener()
        {
        	@Override
        	public void actionPerformed(ActionEvent e)
        	{

        
        		ArrayList<FarmComponent> IC = new ArrayList<FarmComponent>();
        	
        		IC.add(fm);
        	        		
        		JFrame frame = new JFrame();
        		VisualizerComponent visualize = new VisualizerComponent(IC);
        		frame.setSize(300, 300);
        		frame.setTitle("Map of the Farm");
        		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        	    
        		frame.add(visualize);
        		frame.setVisible(true);       
        		              
            }
        });		
	
    }

    
    
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}