import java.awt.*;

public class Item extends FarmComponent{

	private static String name;
	private static int price;
	private static int location_x; 
	private static int location_y;
	private static int length;
	private static int width;
	
	
	public Item(String nm, int p, int x, int y, int l, int w){
		name = nm;
		price = p;	
		location_x = x;
		location_y = y;
		length = l;
		width = w;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		name = n;
	}
	
	public int getPrice(){
		return price;
	}
	
	public void setPrice(int p){
		price = p;
	}
	
	public int getLocationX(){
		return location_x;
	}
	
	public void setLocationX( int x){
		location_x = x;
	}
	
	public int getLocationY(){
		return location_y;
	}
	
	public void setLocationY(int y){
		location_y = y;
	}
	
	public int getLength(){
		return length;
	}
	
	public void setLength(int l){
		length = l;
	}
	
	public int getWidth(){
		return width;
	}
	
	public void setWidth(int w){
		width = w;
	}
	
	public void displayItemInfo() {
		System.out.println(getName() + " " + getPrice());
	}
	
	public String toString() {
        return name;
    }
}