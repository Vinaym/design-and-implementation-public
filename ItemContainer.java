import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
	
public class ItemContainer extends FarmComponent{

	private String name;
	private int price;
	private int location_x;
	private int location_y;
	private int length;
	private int width;
	private ArrayList FarmComponents = new ArrayList();
	
	public ItemContainer(String nm, int p, int x, int y, int l, int w){
		name = nm;
		price = p;	
		location_x = x;
		location_y = y;
		length = l;
		width = w;
	}
	

	public void addNewObject(ItemContainer i){
		FarmComponents.add(i);
	}	
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		name = n;
	}
	
	public int getPrice(){
		return price;
	}
	
	public void setPrice(int p){
		price = p;
	}
	
	public int getLocationX(){
		return location_x;
	}
	
	public void setLocationX(int x){
		location_x = x;
	}
	
	public int getLocationY(){
		return location_y;
	}
	
	public void setLocationY(int y){
		location_y = y;
	}
	
	public int getLength(){
		return length;
	}
	
	public void setLength(int l){
		length = l;
	}
	
	public int getWidth(){
		return width;
	}
	
	public void setWidth(int w){
		width = w;
	}
	
	
	public void add(FarmComponent newFarmComponent)
	{
		FarmComponents.add(newFarmComponent);
	}
	
	public void remove(FarmComponent newFarmComponent)
	{
		FarmComponents.remove(newFarmComponent);
	}
	
	public FarmComponent getComponent(int componentIndex) {
		return(FarmComponent)FarmComponents.get(componentIndex);
	}
	
	public void displayItemInfo() {
		System.out.println(getName()+" " + getPrice() + "\n");
		Iterator itemIterator = FarmComponents.iterator();	
		
		while(itemIterator.hasNext()) {
			FarmComponent itemInfo = (FarmComponent) itemIterator.next();
			itemInfo.displayItemInfo();
		}
	}
	
	public String toString() {
        return name;
    }
	
}